package com.andres.projectpqr.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties.Producer;
import org.springframework.stereotype.Service;

import com.andres.projectpqr.model.Pqr;
import com.andres.projectpqr.repository.PqrRepository;

@Service
@Transactional
public class PqrService {
	
	@Autowired
	PqrRepository pqrRepository;
	
	public List<Pqr> list(){
		return pqrRepository.findAll();
	}
	
	public Optional<Pqr> getOne(int id){
		return pqrRepository.findById(id);
	}
	
	public Optional<Pqr> getByCodPqr(String codPqr){
		return pqrRepository.findByCodPqr(codPqr);
	}

	public void save(Pqr pqr) {
		pqrRepository.save(pqr);
	}
	
	public void delete(int id) {
		pqrRepository.deleteById(id);
	}
	
	public boolean existsById(int id) {
		return pqrRepository.existsById(id);
	}
	
	public boolean existsByCodPqr(String codPqr) {
		return pqrRepository.existsByCodPqr(codPqr);
	}

}
