package com.andres.projectpqr.security.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.andres.projectpqr.security.enums.RoleName;
import com.andres.projectpqr.security.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer>{
	Optional<Role> findByRoleName(RoleName roleName);
}
