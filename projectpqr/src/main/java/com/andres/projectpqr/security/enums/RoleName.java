package com.andres.projectpqr.security.enums;

public enum RoleName {
	ROLE_ADMIN, ROLE_USER
}
