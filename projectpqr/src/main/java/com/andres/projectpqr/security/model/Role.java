package com.andres.projectpqr.security.model;

import javax.persistence.*;

import com.andres.projectpqr.security.enums.RoleName;
import com.sun.istack.NotNull;

@Entity
@Table(name = "roles")
public class Role {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@NotNull
	@Column(name = "name_roles")
	@Enumerated(EnumType.STRING)
	private RoleName roleName;
	
	public Role() {
	}

	public Role(@NotNull RoleName roleName) {
		this.roleName = roleName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public RoleName getRoleName() {
		return roleName;
	}

	public void setRoleName(RoleName roleName) {
		this.roleName = roleName;
	}
	
	
	
	
}
