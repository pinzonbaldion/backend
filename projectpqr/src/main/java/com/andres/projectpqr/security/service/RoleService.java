package com.andres.projectpqr.security.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.andres.projectpqr.security.enums.RoleName;
import com.andres.projectpqr.security.model.Role;
import com.andres.projectpqr.security.repository.RoleRepository;

@Service
@Transactional
public class RoleService {
	
	@Autowired
	RoleRepository roleRepository;
	
	public Optional<Role> getByRoleName(RoleName roleName){
		return roleRepository.findByRoleName(roleName);
	}

	public void save(Role role) {
		roleRepository.save(role);
	}
}
