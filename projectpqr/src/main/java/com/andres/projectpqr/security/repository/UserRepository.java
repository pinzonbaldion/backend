package com.andres.projectpqr.security.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.andres.projectpqr.security.model.User;


@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	Optional<User> findByUserName(String userName);
	boolean existsByUserName(String userName);
	boolean existsByEmail(String email);
}
