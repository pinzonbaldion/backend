package com.andres.projectpqr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectpqrApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectpqrApplication.class, args);
	}

}
