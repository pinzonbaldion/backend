package com.andres.projectpqr.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.andres.projectpqr.model.Pqr;

@Repository
public interface PqrRepository extends JpaRepository<Pqr, Integer>{
	Optional<Pqr> findByCodPqr(String codPqr);
	boolean existsByCodPqr(String codPqr);

}
