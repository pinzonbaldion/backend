package com.andres.projectpqr.dto;

import java.util.Date;
import javax.validation.constraints.NotBlank;

public class PqrDto {
	
	@NotBlank
	private String codPqr;
	private String tipo;
	private String asunto;
	private String usuario;
	private String estado;
	private Date fecCre;
	private Date fecLimt;
	
	
	public PqrDto() {
	}

	public PqrDto(String codPqr, String tipo, String asunto, String usuario, String estado, Date fecCre, Date fecLimt) {
		this.codPqr = codPqr;
		this.tipo = tipo;
		this.asunto = asunto;
		this.usuario = usuario;
		this.estado = estado;
		this.fecCre = fecCre;
		this.fecLimt = fecLimt;
	}

	public String getCodPqr() {
		return codPqr;
	}

	public void setCodPqr(String codPqr) {
		this.codPqr = codPqr;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFecCre() {
		return fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecLimt() {
		return fecLimt;
	}

	public void setFecLimt(Date fecLimt) {
		this.fecLimt = fecLimt;
	}
	
	

}
