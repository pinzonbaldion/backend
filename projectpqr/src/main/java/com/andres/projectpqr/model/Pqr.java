package com.andres.projectpqr.model;

import java.util.Date;

import javax.persistence.*;


@Entity
@Table(name = "pqr")
public class Pqr {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "cod_pqr")
	private String codPqr;
	@Column(name = "tipo")
	private String tipo;
	@Column(name = "asunto")
	private String asunto;
	@Column(name = "usuario")
	private String usuario;
	@Column(name = "estado")
	private String estado;
	@Column(name = "fec_cre")
	private Date fecCre;
	@Column(name = "fec_limt")
	private Date fecLimt;
	
	public Pqr() {
	}

	public Pqr(String codPqr, String tipo, String asunto, String usuario, String estado, Date fecCre, Date fecLimt) {
		this.codPqr = codPqr;
		this.tipo = tipo;
		this.asunto = asunto;
		this.usuario = usuario;
		this.estado = estado;
		this.fecCre = fecCre;
		this.fecLimt = fecLimt;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getCodPqr() {
		return codPqr;
	}

	public void setCodPqr(String codPqr) {
		this.codPqr = codPqr;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFecCre() {
		return fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecLimt() {
		return fecLimt;
	}

	public void setFecLimt(Date fecLimt) {
		this.fecLimt = fecLimt;
	}
	
	
}
