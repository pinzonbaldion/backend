package com.andres.projectpqr.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.apache.commons.lang3.StringUtils;

import com.andres.projectpqr.dto.Message;
import com.andres.projectpqr.dto.PqrDto;
import com.andres.projectpqr.model.Pqr;
import com.andres.projectpqr.services.PqrService;



@RestController
@RequestMapping("/pqr")
//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "*")
public class PqrController {
	
	@Autowired
	PqrService pqrService;
	
	@GetMapping("/pqrList")
	public ResponseEntity<List<Pqr>> list(){
		List<Pqr> list = pqrService.list();
		return new ResponseEntity<List<Pqr>>(list,HttpStatus.OK);
	}
	
	@GetMapping("/pqrDetail/{id}")
	public ResponseEntity<Pqr> getById(@PathVariable("id") int id){
		if(!pqrService.existsById(id)) {
			return new ResponseEntity(new Message("No existe la PQR"),HttpStatus.NOT_FOUND);
		}
		Pqr pqr = pqrService.getOne(id).get();
		return new ResponseEntity(pqr, HttpStatus.OK);
	}
	
	@GetMapping("/pqrDetailCod/{codPqr}")
	public ResponseEntity<Pqr> getByCodPqr(@PathVariable("codPqr") String codPqr){
		if(!pqrService.existsByCodPqr(codPqr)) {
			return new ResponseEntity(new Message("No existe la PQR"),HttpStatus.NOT_FOUND);
		}
		Pqr pqr = pqrService.getByCodPqr(codPqr).get();
		return new ResponseEntity(pqr, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/pqrCreate")
	public ResponseEntity<?> create(@RequestBody PqrDto pqrDto){
		if(StringUtils.isBlank(pqrDto.getCodPqr()))
			return new ResponseEntity(new Message("El codigo no puede estar en blanco"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(pqrDto.getTipo()))
			return new ResponseEntity(new Message("El tipo de PQR no puede estar en blanco"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(pqrDto.getAsunto()))
			return new ResponseEntity(new Message("El asunto de la PQR no puede estar en blanco"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(pqrDto.getUsuario()))
			return new ResponseEntity(new Message("El usuario no puede estar en blanco"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(pqrDto.getEstado()))
			return new ResponseEntity(new Message("El estado no puede estar en blanco"), HttpStatus.BAD_REQUEST);
		if(pqrService.existsByCodPqr(pqrDto.getCodPqr()))
			return new ResponseEntity(new Message("Ya existe esa PQR"), HttpStatus.BAD_REQUEST);
		Pqr pqr = new Pqr(pqrDto.getCodPqr(),pqrDto.getTipo(),pqrDto.getAsunto(),pqrDto.getUsuario(),pqrDto.getEstado(),
				pqrDto.getFecCre(),pqrDto.getFecLimt());
		pqrService.save(pqr);
		return new ResponseEntity<>(new Message("PQR registrada"),HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/pqrUpdate/{id}")
	public ResponseEntity<?> update(@PathVariable("id")int id,@RequestBody PqrDto pqrDto){
		if (!pqrService.existsById(id))
			return new ResponseEntity<>(new Message("La PQR no existe"),HttpStatus.NOT_FOUND);
		if(StringUtils.isBlank(pqrDto.getCodPqr()))
			return new ResponseEntity(new Message("El codigo no puede estar en blanco"), HttpStatus.BAD_REQUEST);
		if(pqrService.existsByCodPqr(pqrDto.getCodPqr()) && pqrService.getByCodPqr(pqrDto.getCodPqr()).get().getId() != id)
			return new ResponseEntity(new Message("El codigo ya existe"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(pqrDto.getTipo()))
			return new ResponseEntity(new Message("El tipo de PQR no puede estar en blanco"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(pqrDto.getAsunto()))
			return new ResponseEntity(new Message("El asunto de la PQR no puede estar en blanco"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(pqrDto.getUsuario()))
			return new ResponseEntity(new Message("El usuario no puede estar en blanco"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(pqrDto.getEstado()))
			return new ResponseEntity(new Message("El estado no puede estar en blanco"), HttpStatus.BAD_REQUEST);
		if(pqrDto.getEstado().equals("C"))
			return new ResponseEntity(new Message("La PQR se encuentra Cerrada. No se Puede Modificar"), HttpStatus.BAD_REQUEST);
		if(!pqrService.existsByCodPqr(pqrDto.getCodPqr()))
			return new ResponseEntity(new Message("Ya existe esa PQR"), HttpStatus.BAD_REQUEST);
		Pqr pqr = pqrService.getOne(id).get();
		pqr.setCodPqr(pqrDto.getCodPqr());
		pqr.setTipo(pqrDto.getTipo());
		pqr.setAsunto(pqrDto.getAsunto());
		pqr.setUsuario(pqrDto.getUsuario());
		pqr.setEstado(pqrDto.getEstado());
		pqr.setFecCre(pqrDto.getFecCre());
		pqr.setFecLimt(pqrDto.getFecLimt());
		pqrService.save(pqr);
		return new ResponseEntity<>(new Message("PQR actualizada"),HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/pqrDelete/{id}")
	public ResponseEntity<?> delete(@PathVariable("id")int id){
		if(!pqrService.existsById(id))
			return new ResponseEntity(new Message("No existe la PQR"),HttpStatus.NOT_FOUND);
		pqrService.delete(id);
		return new ResponseEntity(new Message("PQR Eliminada"),HttpStatus.OK);
	}
	 

}
